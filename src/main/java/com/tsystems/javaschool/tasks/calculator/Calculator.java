package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Calculator {

    private final int NO_PRIORITY = 999;
    private final int PLUS_PRIORITY = 1;
    private final int MULT_PRIORITY = 2;
    private final int MAX_PRIORITY = 2;

    DecimalFormatSymbols dfs = new DecimalFormatSymbols();
    DecimalFormat df = new DecimalFormat("##0.####");

    private Map<Character, Integer> validSymbols = new HashMap<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement == null || statement.isEmpty()) return null;
        
        statement = prepareExpression(statement);

        if (!checkExpression(statement)) return null;

        statement = unaryOpToBinaryOp(statement);

        Double result = calcExpression(statement);

        if (result == null) return null;

        result = new BigDecimal(result).setScale(4, RoundingMode.HALF_UP).doubleValue();

        return df.format(result);
    }

    //Initialization

    {
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);

        initValidSymbolsMap();
    }

    private void initValidSymbolsMap() {
        validSymbols.put('0', NO_PRIORITY);
        validSymbols.put('1', NO_PRIORITY);
        validSymbols.put('2', NO_PRIORITY);
        validSymbols.put('3', NO_PRIORITY);
        validSymbols.put('4', NO_PRIORITY);
        validSymbols.put('5', NO_PRIORITY);
        validSymbols.put('6', NO_PRIORITY);
        validSymbols.put('7', NO_PRIORITY);
        validSymbols.put('8', NO_PRIORITY);
        validSymbols.put('9', NO_PRIORITY);
        validSymbols.put('.', NO_PRIORITY);
        validSymbols.put('(', NO_PRIORITY);
        validSymbols.put(')', NO_PRIORITY);
        validSymbols.put('+', PLUS_PRIORITY);
        validSymbols.put('-', PLUS_PRIORITY);
        validSymbols.put('*', MULT_PRIORITY);
        validSymbols.put('/', MULT_PRIORITY);
    }

    private String prepareExpression(String statement) {

        if (statement == null || statement.isEmpty()) return null;

        String result = statement.trim();
        result = result.replace(" ", "");

        return result;
    }

    private String unaryOpToBinaryOp(String expr) {

        if (validSymbols.get(expr.charAt(0)) == PLUS_PRIORITY) expr = "0" + expr;

        for (int i = 1; i < expr.length(); i++) {
            if (validSymbols.get(expr.charAt(i)) == PLUS_PRIORITY && expr.charAt(i-1) == '(') {
                expr = expr.substring(0, i) + "0" + expr.substring(i, expr.length());
                i++;
            }
        }

        return expr;
    }

    //Checking
    private boolean checkExpression(String expr) {
        return checkExprForValidSymbols(expr) && checkParentheses(expr);
    }

    private boolean checkExprForValidSymbols(String expr) {

        for (char c: expr.toCharArray()) {
            if (validSymbols.get(c) == null) return false;
        }

        return true;
    }

    private boolean checkParentheses(String expr) {

        int parSum = 0;

        for (int i = 0; i < expr.length(); i++) {
            if (expr.charAt(i) == '(') parSum++;
            if (expr.charAt(i) == ')') parSum--;
            if (parSum < 0) return false;
        }

        return true;
    }

    //Parsing
    private String findNextOperand(String expr, int startPosition, int operationPriority) {

        if (expr == null || expr.isEmpty() || startPosition < 0 || startPosition >= expr.length()) return null;

        int parSum = 0;
        int nextSymbolIndex = startPosition;

        while (nextSymbolIndex < expr.length()) {

            char nextSymbol = expr.charAt(nextSymbolIndex);

            if (nextSymbol == '(') parSum++;
            if (nextSymbol == ')') parSum--;

            if (parSum < 0) {
                break;
            }

            if (parSum == 0 && validSymbols.get(nextSymbol) < NO_PRIORITY) {
                if (validSymbols.get(nextSymbol) <= operationPriority) {
                    break;
                }
            }

            nextSymbolIndex++;
        }

        return expr.substring(startPosition, nextSymbolIndex);

    }

    //Calculating
    private Double calcExpression(String expr) {

        if (expr == null || expr.isEmpty()) return null;

        Double result;

        try {
            result = Double.valueOf(expr);
            return result;
        } catch (NumberFormatException e) {
            result = null;
        }

        String leftOperand = null;
        String rightOperand = null;

        Double leftResult = null;
        Double rightResult = null;

        int index = 0;
        int opPriority = 1;

        while (opPriority <= MAX_PRIORITY) {
            leftOperand = findNextOperand(expr, index, opPriority);
            if (leftOperand.length() == expr.length())
                opPriority++;
            else break;
        }

        if (opPriority > MAX_PRIORITY)
            if (expr.charAt(0) == '(' && expr.charAt(expr.length() - 1) == ')') {
                leftOperand = expr.substring(1, expr.length() - 1);
                return calcExpression(leftOperand);
            } else return null;


        leftResult = calcExpression(leftOperand);
        if (leftResult == null) return null;

        index += leftOperand.length();

        while (index < expr.length()) {

            rightOperand = findNextOperand(expr, index + 1, opPriority);

            rightResult = calcExpression(rightOperand);
            if (rightResult == null) return null;

            switch (expr.charAt(index)) {
                case '+': {
                    result = leftResult + rightResult;
                    break;
                }
                case '-': {
                    result = leftResult - rightResult;
                    break;
                }
                case '*': {
                    result = leftResult * rightResult;
                    break;
                }
                case '/': {
                    if (rightResult != 0) result = leftResult / rightResult;
                    else result = null;
                    break;
                }
                default: {
                    result = null;
                }
            }

            index += rightOperand.length() + 1;
            leftResult = result;
        }

        return result;
    }

}
