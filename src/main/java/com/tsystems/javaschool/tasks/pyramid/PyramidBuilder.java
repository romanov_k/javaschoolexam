package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers == null) throw  new CannotBuildPyramidException();

        Integer pyramidHeight = checkPyramidHeight(inputNumbers.size());
        if (pyramidHeight == null || pyramidHeight == 0) throw new CannotBuildPyramidException();

        int[][] pyramid = new int[pyramidHeight][pyramidHeight * 2 - 1];

        inputNumbers.sort(comparator);

        int pyramidMiddle = pyramid[0].length / 2;
        int nextFloorElementStartIndex = 0;

        for (int i = 0; i < pyramidHeight; i++) {
            for (int j = 0; j <= i; j++) {
                pyramid[i][pyramidMiddle - i + 2 * j] = inputNumbers.get(nextFloorElementStartIndex + j);
            }
            nextFloorElementStartIndex += i + 1;
        }

        return pyramid;
    }

    private Integer checkPyramidHeight(int arraySize) {

        if (arraySize <= 0) return null;
        if (arraySize == 1) return 1;

        double floatHeight = (-1 + Math.sqrt(1 + 4*2*arraySize)) / 2;
        long roundedFloatHeight = Math.round(floatHeight);

        if (roundedFloatHeight == floatHeight) return (int) roundedFloatHeight;

        return null;
    }

    private Comparator<Integer> comparator = (Comparator<Integer>) (o1, o2) -> {
        if (o1 == null || o2 == null) throw new CannotBuildPyramidException();
        if (o1 > o2) return 1;
        if (o1 < o2) return -1;
        return 0;
    };

}
